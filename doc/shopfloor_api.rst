Shopfloor API (TODO)
====================

Overview
--------

The :py:class:`cros.factory.shopfloor.ShopFloorBase` class implements common
base to support various shopfloor servers and product requirements.

Extending the ShopFloorBase class
---------------------------------

API Documentation
-----------------

.. py:module:: cros.factory.shopfloor

.. autoclass:: ShopFloorBase
   :members:
