// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

$(document).ready(function() {
  $('#devices_button').button({
    icons: {primary: 'ui-icon-home'}
  });

  $('#query_button').button({
    icons: {primary: 'ui-icon-search'}
  });

  $('#device_button').button({
    icons: {primary: 'ui-icon-info'}
  });

  $('#hwids_button').button({
    icons: {primary: 'ui-icon-disk'}
  });

  $('#tests_button').button({
    icons: {primary: 'ui-icon-clipboard'}
  });

});
