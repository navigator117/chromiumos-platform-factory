# Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import os
import sys
sys.path.append(
    os.path.dirname(
        os.path.realpath(__file__.replace('.pyc', '.py'))))
