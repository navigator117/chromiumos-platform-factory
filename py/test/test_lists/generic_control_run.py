# -*- mode: python; coding: utf-8 -*-
#
# Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.


"""Defines control run units serial numbers."""


# A list of strings containing image update control run serial numbers.
CONTROL_RUN_IMAGE_UPDATE_SERIAL_NUMBERS = []
# A list of strings containing firmware update control run serial numbers.
CONTROL_RUN_FIRMWARE_UPDATE_SERIAL_NUMBERS = []
