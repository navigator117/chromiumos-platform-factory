# Copyright (c) 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

UMPIRE_VERSION_MAJOR = 3
UMPIRE_VERSION_MINOR = 0
